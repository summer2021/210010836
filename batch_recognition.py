from task_1 import Task_1
from task_2 import Task_2
from task_3 import Task_3
from task_4 import Task_4
import os
def main():
    print("请将需要批量处理的rpm包放入指定文件夹中")
    input_file = input()
    for root, dirs, files in os.walk(input_file):
        for file in files:
            abs_file = Task_1(file)
            start_command, restart_command, stop_command = Task_2(abs_file)
            sys_command = Task_3(abs_file)
            ans, other = Task_4(sys_command)

if __name__ == '__main__':
    main()